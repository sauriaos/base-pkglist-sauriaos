#
# spec file for package plasma5-pkglist-sauriaos
#
# Copyright (c) 2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


%define __find_requires perl %{SOURCE1}
%define _use_internal_dependency_generator 0
Name:           plasma5-pkglist-sauriaos
Version:        5.9.2
Release:        0
Summary:        KDE Stuff for home:SauriaOS
License:        BSD-3-Clause
Group:          Metapackages
Url:            http://en.opensuse.org/Patterns
Source0:        packagelist
Source1:        list-requires
Source2:        plasma5-pkglist-rpmlintrc
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildArch:      noarch

%description
Installation of this package does not make sense.

%prep

%build

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_datadir}/suse/packages/plasma5_stable.list

%files
%defattr(-,root,root)
%dir %{_datadir}/suse
%dir %{_datadir}/suse/packages
%{_datadir}/suse/packages/plasma5_stable.list

%changelog
